$apps = @(
  #'vscode'
  'rocketchat'
  '7zip'
  #'nextcloud-client'
  'HeidiSQL'
)

Write-Host 'Installing Apps:'
for ($i = 0; $i -lt $apps.length; $i++) {
  Write-Host '  installing '$apps[$i]
  Invoke-Expression 'choco install ' + $apps[$i]
}

