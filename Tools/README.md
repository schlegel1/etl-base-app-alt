# Tools

## Voraussetzungen

Für einige Scripts müssen bestimmte Voraussetzungen gegeben sein.

### Lizenzen

Lizenzen für BC müssen ins Verzeichnis Licenses kopiert werden. Die Inhalte dieses Verzeichnisses werden nicht automatisch in das Source Code Repository übernommen.

## Scripts

Die Scripts lassen sich ausführen wenn man in das Tools Verzeichnis dieser App wechselt und eine Powershell Konsole startet und die Scripts dort ausführt (ggf. Admin Rechte beachten).

### Lokale Server neu starten

```Powershell
cd Tools
.\restart_server.ps1
```

