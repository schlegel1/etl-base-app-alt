# Dieses Tools installiert nach einem Sprint alle Apps die im App Repository Verzeichnis liegen
# Die neu zu installierenden Apps liegen im App Repository\install Verzeichnis.
# Wenn es ein neues Release gibt muss der Inhalt dieses Verzeichnis an einen anderen Ort kopiert werden.

# Alle Powershell Module für BC laden
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Apps.Management.psd1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Apps.Tools.psd1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Management.psd1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\NavAdminTool.ps1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\NavModelTools.ps1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Management.psm1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\NAVWebClientManagement.psm1"


# Install-NAVApp -force
# Install-NAVApp -force -ServerInstance BC -Path '.\Proseware SmartApp.app' -Tenant 'Tenant1'