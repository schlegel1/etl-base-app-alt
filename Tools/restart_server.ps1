# alle lokalen und laufenden BC Instanzen neu starten.

# Alle Powershell Module für BC laden
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Apps.Management.psd1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Apps.Tools.psd1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Management.psd1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\NavAdminTool.ps1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\NavModelTools.ps1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Management.psm1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\NAVWebClientManagement.psm1"
$Instances = Get-NAVServerInstance  | where { $_.DisplayName -Like "*BC*" -and $_.State -eq "Running" } | select -expand ServerInstance 

Write-Output "Alle laufenden BC Instanzen werden neu gestartet:"
Foreach ($Instance in $Instances) {
  Write-Output "  Starte $Instance neu."
  Restart-NAVServerInstance -ServerInstance $Instance -Verbose
}

Write-Output "Serverinstanzen wurden neu gestartet."