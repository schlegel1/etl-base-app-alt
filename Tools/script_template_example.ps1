# Alle Powershell Module für BC laden
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Apps.Management.psd1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Apps.Tools.psd1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Management.psd1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\NavAdminTool.ps1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\NavModelTools.ps1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\Microsoft.Dynamics.Nav.Management.psm1"
Import-Module -Name "C:\Program Files\Microsoft Dynamics 365 Business Central\190\Service\NAVWebClientManagement.psm1"

# Standard Variablen zuweisen

$LicenseDir = '..\Licenses'
$Instances = Get-NAVServerInstance  | where { $_.DisplayName -Like "*BC*" -and $_.State -eq "Running" } | select -expand ServerInstance 

# Beispiel für eine Iteration
# Foreach ($Instance in $Instances) {
#   # Write-Output $Instance
#   Export-NAVServerLicenseInformation $Instance
# }

$latestLicenseFile = Get-ChildItem -Path $LicenseDir | where { $_.Extension -eq '.flf' } | Sort-Object LastAccessTime -Descending | Select-Object -First 1

# Funktionen
function UpdateLicense {
  Foreach ($Instance in $Instances) {
    # Write-Output $Instance
    Write-Output $latestLicenseFile.FullName
    Export-NAVServerLicenseInformation $Instance
  }
}

# Funktionen starten
UpdateLicense