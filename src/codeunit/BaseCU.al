dotnet
{
    assembly(mscorlib)
    {
        type(System.DateTime; MyDateTime) { }
        type(System.IO.File; DotnetFile) { }
    }
}

codeunit 50001 ETL_BaseCU
{
    // Spielkram mit dotnet
    procedure ShowDotNetTime()

    var
        now: DotNet MyDateTime;
        MyFile: DotNet DotnetFile;
    begin
        //now := now.UtcNow();
        // Message('Hello, world! It is: ' + now.ToString());

        if MyFile.Exists('\\etlshares.schlegel.intern\public\test.csv') then
            Message('Datei wurde gefunden')
        else
            Message('Nicht gefunden');
    end;

    procedure ImportFile()
    var
        FileTest: File;
        StreamInTest: Instream;
        Txt: Text;
        Int: Integer;
    begin
        FileTest.Open('c:\XMLDocs\NewTest.txt');
        FileTest.CreateInStream(StreamInTest);
        // Starting a loop
        while not (StreamInTest.EOS) do begin
            Int := StreamInTest.ReadText(Txt, 100);
            Message(Txt + '\Size: ' + Format(Int));
        end;
        FileTest.Close();
    end;

}
