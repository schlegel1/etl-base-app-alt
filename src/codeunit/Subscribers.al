codeunit 50003 ETL_Subscribers
{
    EventSubscriberInstance = StaticAutomatic;

    [EventSubscriber(ObjectType::Page, Page::"Customer Card", 'OnBeforeValidateEvent', 'Address', true, true)]

    local procedure CheckAddressLineOnBeforeValidateEvent(var Rec: Record Customer)
    begin
        Message('OnBeforeValidateEvent über CheckAddressLineOnBeforeValidateEvent 1 getriggert', Rec.Address);
    end;

    [EventSubscriber(ObjectType::Page, Page::"Customer Card", 'OnBeforeValidateEvent', 'Address', true, true)]
    local procedure CheckAddressLineOnBeforeValidateEvent2(var Rec: Record Customer)
    begin
        Message('OnBeforeValidateEvent über CheckAddressLineOnBeforeValidateEvent 2 getriggert', Rec.Address);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::ETL_Publishers, 'MyPublishedEvent', '', true, true)]
    local procedure ProcessMyPublishedEvent(line: Text[100])
    begin
        message('unser published event wurde ausgeführt %1 ' + line);
    end;

}
