page 50001 ETL_BasePage
{
    Caption = 'ETLBasePage', Comment = 'ETL Startmenü';
    PageType = Card;

    layout
    {
        area(content)
        {
            group(General)
            {

            }
        }
    }

    actions
    {
        area(Processing)
        {

            group(Process)
            {
                action(TestDotnet)
                {
                    Caption = 'ShowDotNetTime', Comment = 'ShowDotNetTime';
                    Image = Find;
                    ApplicationArea = All;

                    trigger OnAction()
                    var
                        Publisher: Codeunit ETL_Publishers;
                    begin
                        ETLCU.ShowDotNetTime();
                        Publisher.MyPublishedEvent('Test')
                    end;
                }
            }
            group(Check)
            {
                action(CheckAction)
                { }
            }
        }
    }
    var
        ETLCU: Codeunit ETL_BaseCU;
}
