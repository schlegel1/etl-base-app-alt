page 50000 ETL_DepartmentIT
{
    Caption = 'ETL Price Info Department', Comment = 'ETL Preisinfo Abteilung';
    PageType = RoleCenter;

    layout
    {
        area(RoleCenter)
        {
            part(MyJobQueue; "My Job Queue")
            {
                ApplicationArea = All;
            }
            systempart(MyNotes; MyNotes)
            {
                ApplicationArea = All;
            }
            systempart(Notes; Notes)
            {
                ApplicationArea = All;
            }
            systempart(Outlook; Outlook)
            {
                ApplicationArea = All;
                Visible = false;
            }
        }
    }

    actions
    {
        area(Sections)
        {
            group(StartGrp)
            {
                Caption = 'Bereiche', Comment = 'Startmenü';
                action(VendorMasterData)
                {
                    Caption = 'Preispflege', Comment = 'Lieferantenstammdaten';
                    ApplicationArea = All;
                    RunObject = page KVSPI_VendorMasterData;
                }
            }
            group(PriceInformation)
            {
                Caption = 'Sonstiges', Comment = 'Sonstiger Kram';
            }
        }
    }

    var
        ETLCU: Codeunit ETL_BaseCU;
}